set(RCJSON_DIR ${PROJECT_SOURCE_DIR}/src/sources/rc_json)
set(RC_JSON_FILES
        ${RCJSON_DIR}/json_reader.cpp
        ${RCJSON_DIR}/json_tool.h
        ${RCJSON_DIR}/json_value.cpp
        ${RCJSON_DIR}/json_valueiterator.inl
        ${RCJSON_DIR}/json_writer.cpp
        )

