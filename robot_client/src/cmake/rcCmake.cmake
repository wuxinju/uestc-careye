set(LIB_NAME rc)
set(RC_FILES
        ${RC_SERAIL_FILES}
        ${RC_TASK_FILES}
        ${RC_RTMP_FILES}
        ${RC_MOVE_FILES}
        ${RC_MAPPING_FILES}
        ${RC_MESSAGE_FILES}
        ${RC_GUI_FILES}
        ${RC_CV_FILES}
        ${RC_JSON_FILES}
        ${RC_LOG_FILES}
        ${RC_NNET_FILES}
        ${RC_NETWORK_FILES}
        ${RC_GPIO_FILES}
        )

add_library(rc_grpc_proto
        ${PROTO_SRC}
        ${PROTO_H})
target_link_libraries(rc_grpc_proto
        libprotobuf
        pthread
        grpc++
        grpc++_alts
        grpc++_unsecure
        grpc++_error_details
        grpc++_reflection
        )
add_library(${LIB_NAME} SHARED ${RC_FILES})
target_link_libraries(${LIB_NAME} ${OpenCV_LIBS}
        boost_system
        boost_thread
        pthread
        ${OPENGL_LIBRARIES}
        ${GLUT_LIBRARY}
        glib-2.0
        stdc++
        m
        ${MPI_CXX_LIBRARIES}
        ${realsense2_LIBRARY}
        ${Pistache_LIBRARIES}/libpistache.a
        libserv
        libprotobuf
        rc_grpc_proto
        )
add_dependencies(${LIB_NAME} libprotobuf protoc grpc++)
if (${WITH_LIBREALSENCE2} STREQUAL "OFF")
    target_link_libraries(${LIB_NAME} realsense2)
endif ()
if (${WITH_TBB} STREQUAL "ON")
    target_link_libraries(${LIB_NAME} ${TBB_IMPORTED_TARGETS})
endif ()

