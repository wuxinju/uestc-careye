message(STATUS "Load Mapping Module")
set(RCMAPPING_DIR ${PROJECT_SOURCE_DIR}/src)

file(GLOB RC_MAPPING_FILES ${RCMAPPING_DIR}/sources/rc_mapping/*.cpp)
set(RC_MAPPING_FILES
        ${RC_MAPPING_FILES}
        ${RCSLAMRADAR_FILES}
        )
foreach(FILE ${RC_MAPPING_FILES})
    message(STATUS "RC_MAPPING:${FILE}")
endforeach()
include_directories(${RCMAPPING_DIR}/include)

