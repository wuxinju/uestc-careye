//
// Created by PulsarV on 18-10-30.
//

#include <rc_message/rc_base_msg.hpp>
#include <map>
#include <rc_message/rc_image_msg.h>


namespace RC {
    namespace Message {
        ImageMessage::ImageMessage(int max_queue_size):BaseMessage<std::string>(max_queue_size) {

        }
        boost::mutex ImageMessage::image_mutex;

    }
}