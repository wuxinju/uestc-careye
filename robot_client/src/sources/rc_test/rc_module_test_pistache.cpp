//
// Created by Pulsar on 2020/5/16.
//


#include <pistache/endpoint.h>

using namespace Pistache;

struct HelloHandler : public Http::Handler {
HTTP_PROTOTYPE(HelloHandler)
    void onRequest(const Http::Request&, Http::ResponseWriter writer) override{
        writer.send(Http::Code::Ok, "Hello, World!");
    }
};

int main() {
    Http::listenAndServe<HelloHandler>(Pistache::Address("*:9800"));
    return 0;
}