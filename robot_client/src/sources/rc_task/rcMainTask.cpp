//
// Created by Pulsar on 2020/5/16.
//
#include <rc_task/rcTaskManager.h>
#include <iostream>
#include <base/slog.hpp>
namespace RC{
    namespace Task{
        int TaskManager::start_main_task() {
            //自检
            if (this->is_init) {
                if (!this->RobotServer.empty()) {
                    std::map<std::string, void *>::iterator it;
                    it = RobotServer.begin();
                    while (it != RobotServer.end()) {
                        slog::info << "Service: " << it->first << slog::endl;
//                        auto _thread = (rc_Thread *) it->second;
//                _thread->join();
                        it++;
                    }

                } else return false;
            } else {
                slog::err << "You should run init before start" << slog::endl;
                return -1;
            }
            return 0;
        }
    }
}

