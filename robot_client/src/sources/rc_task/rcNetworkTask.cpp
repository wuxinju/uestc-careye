//
// Created by Pulsar on 2020/5/16.
//

#include <rc_task/rcNetworkTask.h>
#include <boost/asio.hpp>
#include <rc_task/rcWebStream.h>
#include <iostream>
#include <rc_network/rc_asny_tcp_client.h>
#include <base/slog.hpp>
namespace RC{
    namespace Task{
        void run_network_task(rc_ServerInfo server_info) {
            slog::info << "正在连接网络...." << slog::endl;
            auto const address = boost::asio::ip::address_v4::from_string(server_info.remote_address);
            boost::asio::ip::tcp::endpoint point(address, server_info.remote_port);
            Network::rc_asny_tcp_client tcpClient(point);
            tcpClient.run();
        }
        void run_websocket_task(rc_ServerInfo server_info){
            slog::info << "启动WEBSOCKET服务...." << slog::endl;

        }
    }
}
