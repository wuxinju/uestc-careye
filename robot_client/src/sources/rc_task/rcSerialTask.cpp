//
// Created by PulsarV on 18-5-14.
//
#include <rc_task/rcSerialTask.h>
#include <rc_message/rc_serial_msg.h>
#include <iostream>
#include <base/slog.hpp>
namespace RC {
    namespace Task {
        void run_serial_task(std::string serial_path){
            slog::info << "串口任务启动中" << slog::endl;
            slog::info << "串口任务启动完成" << slog::endl;
        }
    }
}


