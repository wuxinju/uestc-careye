//
// Created by PulsarV on 18-5-9.
//

#ifndef ROBOCAR_MAIN_H
#define ROBOCAR_MAIN_H

#include <iostream>
#include <rc_move/rcmove.h>
#include "rc_param.h"
#include <rc_log/rclog.h>
#include <fcntl.h>
#include <zconf.h>


#define RC_STRING_SYSTEM_START (char*)"RoboCar系统启动"

#endif //ROBOCAR_MAIN_H
